var gulp        = require('gulp');
var less        = require('gulp-less');
var path        = require('path');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('less', function () {
  return gulp.src('./../src/less/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./css'))
    .pipe(reload({ stream:true }));
});

// watch files for changes and reload
gulp.task('default', function() {
  browserSync({
    server: {
      baseDir: './'
    }
  });

  gulp.watch('./../src/less/*.less', ['less']);
  gulp.watch("*.html", reload);
});